# Luckru test work

В данном репозитории размещено решение заданичи для Luckru

Используемый софт при решении задачи:
- Редактор - Nodepad++
- Локальный сервер - Open Server в конфигурации:
- Apache 2.4 + Nginx 1.14
- PHP 5.6 x64
- MySQL 5.6 x64


 На клиенте:
Как указано в задаче, можно чистый js, можно jquery, счел это намеком на то, что лушче на jquery (просто потому что кода меньше писать в отличие от чистого js).
Так-же речь шла и про bootstrap, поэтому интерфейс сделал на бутcnрапе, подгружается с CDN.

## Задача
Цель: «Разработать Телефонный справочник» 
Сайт должен состоять из одной страницы на которой будет список в таком виде: 
ФИО	Телефон	Кем приходится	Кнопки действий
Тест Тестов Тестович	8 (800) 555 35-35	Банк	[Удалить] [Редактировать]

В интерфейсе должна быть кнопку «Добавить», при нажатии на которую должно открыться диалоговое окно с формой ввода данных. 
Интерфейс Вы верстаете на свои вкус, НО он должен быть дружелюбный к любому пользователю. Он должен быть адаптивный.
Все действия «Добавление». «Удаление», «Редактирование» должно быть реализовано без обновления страницы. Также элементы таблицы при выполнении действий «Удаления», «Добавления», «Редактирования»  должны обновляться БЕЗ ОБНОВЛЕНИЯ СТРАНИЦЫ. 

Базовый стек для разработки:  HTML, CSS (чистый либо препроцессоры, возможен Bootstrap), JS (чистый либо библиотеки/фреймворки), PHP, MySQL, допускается использование webpack для сборки.
