	$(document).on('click','#btn-add',function(e) {
		var data = $("#user_form").serialize();
		/* Добавляем данные в таблицу без обновления 
		id возвращается ajaxом из базы
		остальные данные берутся из формы модального окна
		*/
		var i = $('table tr').length;
		var id = i;
		var name = $('#name').val();
		var email = $('#email').val();
		var phone = $('#phone').val();
		var whois = $('#whois').val();
		var data2tr = `
			<tr id="${id}">
				<td>
						<span class="custom-checkbox">
							<input type="checkbox" class="user_checkbox" data-user-id="${id}">
							<label for="checkbox2"></label>
						</span>
				</td>
				<td>${i}</td>
				<td>${name}</td>
				<td>${email}</td>
				<td>${phone}</td>
				<td>${whois}</td>
				<td>
					<a href="#editEmployeeModal" class="edit" data-toggle="modal">
						<i class="material-icons update" data-toggle="tooltip" 
						data-id="${id}"
						data-name="${name}"
						data-email="${email}"
						data-phone="${phone}"
						data-whois="${whois}"
						title="Edit"></i>
					</a>
					<a href="#deleteEmployeeModal" class="delete" data-id="${id}" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" 
						title="Delete"></i></a>
				</td>
			</tr>`;
		var tableBody = $( "table tbody" );
		$.ajax({
			data: data,
			type: "post",
			url: "backend/save.php",
			success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
					if(dataResult.statusCode==200){
						$('#addEmployeeModal').modal('hide');
						tableBody.append(data2tr);					
					}
					else if(dataResult.statusCode==201){
					   //console.log(dataResult); 
					   console.error('Ошибка сохранкения'); 
					}
			}
		});
	});
	$(document).on('click','.update',function(e) {
		var id=$(this).attr("data-id");
		var name=$(this).attr("data-name");
		var email=$(this).attr("data-email");
		var phone=$(this).attr("data-phone");
		var whois=$(this).attr("data-whois");
		$('#id_u').val(id);
		$('#name_u').val(name);
		$('#email_u').val(email);
		$('#phone_u').val(phone);
		$('#whois_u').val(whois);
	});
	
	$(document).on('click','#update',function(e) {
		var data = $("#update_form").serialize();
		$.ajax({
			data: data,
			type: "post",
			url: "backend/save.php",
			success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
					if(dataResult.statusCode==200){
						$('#editEmployeeModal').modal('hide');
						/**
						Обновить данные
						**/
						console.log('Данные успешно обновленны!'); 
                        					
					}
					else if(dataResult.statusCode==201){
					   console.error("Ошибка обновления: "+dataResult);
					}
			}
		});
	});
	$(document).on("click", ".delete", function() { 
		var id=$(this).attr("data-id");
		$('#id_d').val(id);
		
	});
	$(document).on("click", "#delete", function() { 
		$.ajax({
			url: "backend/save.php",
			type: "POST",
			cache: false,
			data:{
				type:3,
				id: $("#id_d").val()
			},
			success: function(dataResult){
					$('#deleteEmployeeModal').modal('hide');
					$("#"+dataResult).remove();
			
			}
		});
	});
	$(document).on("click", "#delete_multiple", function() {
		var user = [];
		$(".user_checkbox:checked").each(function() {
			user.push($(this).data('user-id'));
		});
		if(user.length <=0) {
			alert("Please select records."); 
		} 
		else { 
			WRN_PROFILE_DELETE = "Вы хоитет удалить "+(user.length>1?"эти данные?":"эту строку?");
			var checked = confirm(WRN_PROFILE_DELETE);
			if(checked == true) {
				var selected_values = user.join(",");
				console.log(selected_values);
				$.ajax({
					type: "POST",
					url: "backend/save.php",
					cache:false,
					data:{
						type: 4,						
						id : selected_values
					},
					success: function(response) {
						var ids = response.split(",");
						for (var i=0; i < ids.length; i++ ) {	
							$("#"+ids[i]).remove(); 
						}	
					} 
				}); 
			}  
		} 
	});
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
		var checkbox = $('table tbody input[type="checkbox"]');
		$("#selectAll").click(function(){
			if(this.checked){
				checkbox.each(function(){
					this.checked = true;                        
				});
			} else{
				checkbox.each(function(){
					this.checked = false;                        
				});
			} 
		});
		checkbox.click(function(){
			if(!this.checked){
				$("#selectAll").prop("checked", false);
			}
		});
	});